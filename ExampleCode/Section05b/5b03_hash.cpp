/**********************************************
* File: 5b02_hash.cpp
* Author: Matthew Morrison
* Email: matt.morrison@nd.edu
* 
* File for the Hash solution to the
* Robot problem 
**********************************************/

#include<iostream>
#include<stdlib.h>
#include<ctime>
#include<unordered_set>
#include "Array.h"

struct Point {
    int row;
    int col;

    /********************************************
    * Function Name  : col
    * Pre-conditions : 0
    * Post-conditions: Point() : row(0),
    *  
    ********************************************/
    Point() : row(0), col(0) {};
	
    /********************************************
    * Function Name  : col
    * Pre-conditions : col
    * Post-conditions: Point(const int& row, const int& col) : row(row),
    *  
    ********************************************/
    Point(const int& row, const int& col) : row(row), col(col) {};
	
    /********************************************
    * Function Name  : Point
    * Pre-conditions : const Point& other
    * Post-conditions: none
    *  
    ********************************************/
    Point(const Point& other){
        row = other.row;
        col = other.col;
    };

    /********************************************
    * Function Name  : operator=
    * Pre-conditions : const Point& other
    * Post-conditions: Point&
    *  
    ********************************************/
    Point& operator=(const Point& other) {
        row = other.row;
        col = other.col;
        return *this;
    };

    /********************************************
    * Function Name  : operator==
    * Pre-conditions : const Point& other
    * Post-conditions: bool
    *  
    ********************************************/
    bool operator==(const Point& other) const {
        if (row == other.row && col == other.col)
            return true;
        return false;
    };

    /********************************************
    * Function Name  : operator<
    * Pre-conditions : const Point& other
    * Post-conditions: bool
    *  
    ********************************************/
    bool operator<(const Point& other) {
        if (row < other.row )
            return true;
        else if (row == other.row && col == other.col)
            return true;

        return false;
    };

    /********************************************
    * Function Name  : operator()
    * Pre-conditions : const Point& pointToHash
    * Post-conditions: size_t
    *  
    ********************************************/
    size_t operator()(const Point& pointToHash) const noexcept {
        size_t hash = pointToHash.row + 10 * pointToHash.col;
        return hash;
    };

};

namespace std {
    template<> struct hash<Point>
    {
        /********************************************
        * Function Name  : operator()
        * Pre-conditions : const Point& p
        * Post-conditions: std::size_t
        * Source: http://stackoverflow.com/50888127/unordered-set-with-custom-struct 
        ********************************************/
        std::size_t operator()(const Point& p) const noexcept
        {
            return p(p);
        }
    };
}


/********************************************
* Function Name  : getPath
* Pre-conditions : bool** maze, size_t rows, size_t cols
* Post-conditions: Array<Point>*
*  
********************************************/
Array<Point>* getPath(bool** maze, size_t rows, size_t cols);


/********************************************
* Function Name  : getPath
* Pre-conditions : bool** maze, int row, int col, Array<Point>* path, std::unordered_set<Point>* failedSet
* Post-conditions: bool
*  
********************************************/
bool getPath(bool** maze, int row, int col, Array<Point>* path, std::unordered_set<Point>* failedSet);

/********************************************
* Function Name  : printInitPath
* Pre-conditions : bool** maze, size_t rows, size_t cols
* Post-conditions: none
*  
********************************************/
void printInitPath(bool** maze, size_t rows, size_t cols);

/********************************************
* Function Name  : printFinalPath
* Pre-conditions : Array<Point>* thepath
* Post-conditions: none
*  
********************************************/
void printFinalPath(Array<Point>* thepath);

/********************************************
* Function Name  : main
* Pre-conditions : int argc, char**argv
* Post-conditions: int
* 
* This is the main driver function
* Assumes argv[1] is a valid integer 
********************************************/
int main(int argc, char**argv){

	const size_t rows = 5; 
	const size_t cols = 6;
	
	bool **maze;
	maze = new bool*[rows]; // Dynamic array (size cols)
	
	for (size_t i = 0; i < rows; ++i) {
		maze[i] = new bool[cols];
	}
	
    for(size_t i = 0; i < rows; i++){
		for(size_t j = 0; j < cols; j++){
			maze[i][j]=true;
		}
    }
	
	srand(time(NULL));
	size_t randomNum = rand()%(rows*cols);
	
	if(randomNum != 0 && randomNum != (rows*cols-1)){
		maze[randomNum%rows][randomNum%cols] = false;
	}
	else{
		maze[rows%2][cols%2] = false;
	}
	
	// Print the path
	std::cout << "Initial Maze" << std::endl;
	printInitPath(maze, rows, cols);
	
	// Call the function
	printFinalPath(getPath(maze, rows, cols));

	return 0;
}

void printInitPath(bool** maze, size_t rows, size_t cols){
	for(size_t i = rows; i > 0; i--){
		for(size_t j = 0; j < cols; j++){
			std::cout << maze[i-1][j] << " ";
		}
		std::cout << std::endl;
	}
}

Array<Point>* getPath(bool** maze, size_t rows, size_t cols){
	Array<Point>* path = new Array<Point>();
	std::unordered_set<Point>* failedSet = new std::unordered_set<Point>();
	
	if(getPath(maze, rows - 1, cols - 1, path, failedSet)){
		return path;
	}
	
	return nullptr;
}

bool getPath(bool** maze, int row, int col, Array<Point>* path, std::unordered_set<Point>* failedSet){
	
	if(col < 0 || row < 0 || !maze[row][col]){
		return false;
	}
	
	Point p(row, col);
	
	std::unordered_set<Point>::const_iterator findIter = failedSet->find(p);
	
	// If the iterator is not at the end, then the Point was found
	if ( findIter != failedSet->end() ){
		std::cout << "Test: " << row << " " << col  << std::endl;
		return false;
	}
	
	bool isAtOrigin = (row == 0) && (col == 0);
	
	if(isAtOrigin || getPath(maze, row, col-1, path, failedSet) || getPath(maze, row-1, col, path, failedSet)){
		path->push_back(p);
		return true;
	}

	return false;
}

void printFinalPath(Array<Point>* thePath){
	
	if(thePath != nullptr){
		std::cout << "Path Found! " << thePath->size() << std::endl;
		size_t pathSize = thePath->size();
		for(size_t i = pathSize; i > 0; i--){
			if(!((*thePath)[i-1].row == 0 && (*thePath)[i-1].col == 0)){
				std::cout << "(" << (*thePath)[i-1].row ;
				std::cout << "," << (*thePath)[i-1].col << ")->"; 
			}
			else{
				std::cout << "(" << (*thePath)[i-1].row ;
				std::cout << "," << (*thePath)[i-1].col << ")"; 
			}
		}
		std::cout << std::endl;
	}
}

